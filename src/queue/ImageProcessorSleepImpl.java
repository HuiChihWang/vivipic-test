package queue;

public class ImageProcessorSleepImpl implements ImageProcessor {
    @Override
    public void process(String path, Runnable callback) {
        Thread sleepThread = new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            callback.run();
        });
        sleepThread.start();
    }
}
