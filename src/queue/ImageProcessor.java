package queue;

public interface ImageProcessor {
    void process(String path, Runnable callback);
}
