package queue;

public interface ProcessQueue {
    boolean isEmpty();
    void addToQueue(String path);
    void removeFromQueue(String path);
}
