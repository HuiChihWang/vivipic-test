package queue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

public class CustomArrayListImpl<Element> implements CustomArrayList<Element> {
    private final List<Element> list = new ArrayList<>();
    @Override
    public int length() {
        return list.size();
    }

    @Override
    public Element get(int index) {
        return list.get(index);
    }

    @Override
    public void insertAt(Element element, int index) {
        list.add(index, element);
    }

    @Override
    public Element removeFrom(int index) {
        return list.remove(index);
    }

    @Override
    public Iterator<Element> iterator() {
        return list.listIterator();
    }

    @Override
    public void forEach(Consumer<? super Element> action) {
        CustomArrayList.super.forEach(action);
    }

    @Override
    public Spliterator<Element> spliterator() {
        return CustomArrayList.super.spliterator();
    }
}
