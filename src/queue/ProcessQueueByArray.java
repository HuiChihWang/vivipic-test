package queue;

public class ProcessQueueByArray implements ProcessQueue {
    private final CustomArrayList<String> listQueue = new CustomArrayListImpl<>();

    private final ImageProcessor imageProcessor;

    private boolean isInProcess = false;

    public ProcessQueueByArray(ImageProcessor imageProcessor) {
        this.imageProcessor = imageProcessor;
    }

    @Override
    public boolean isEmpty() {
        return !isInProcess && listQueue.length() == 0;
    }

    @Override
    public void addToQueue(String path) {
        System.out.println("Add to queue " + path);
        listQueue.insertAt(path, listQueue.length());

        if (!isInProcess) {
            isInProcess = true;
            startProcess();
        }
    }

    private void startProcess() {
        if (listQueue.length() == 0) {
            isInProcess = false;
            return;
        }
        String path = listQueue.removeFrom(0);
        System.out.println("Process image start " + path);
        imageProcessor.process(path, () -> {
            System.out.println("Process image done " + path);
            startProcess();
        });
    }

    @Override
    public void removeFromQueue(String path) {
        for (int i = 0; i < listQueue.length(); i++) {
            if (path.equals(listQueue.get(i))) {
                System.out.println("Remove from queue " + path);
                listQueue.removeFrom(i);
                break;
            }
        }
    }

    public static void main(String[] args) {
        ImageProcessor imageProcessor = new ImageProcessorSleepImpl();
        ProcessQueue processQueue = new ProcessQueueByArray(imageProcessor);

        processQueue.addToQueue("image1.jpg");
        processQueue.addToQueue("image2.jpg");
        processQueue.addToQueue("image3.jpg");
        processQueue.addToQueue("image4.jpg");
        processQueue.addToQueue("image5.jpg");
        processQueue.addToQueue("image6.jpg");
        processQueue.removeFromQueue("image4.jpg");
        processQueue.removeFromQueue("image3.jpg");
        processQueue.removeFromQueue("image1.jpg");
        processQueue.addToQueue("image3.jpg");
        processQueue.addToQueue("image9.jpg");
    }
}
