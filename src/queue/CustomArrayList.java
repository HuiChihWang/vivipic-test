package queue;

public interface CustomArrayList<Element> extends Iterable<Element> {
    int length();
    Element get(int index);
    void insertAt(Element element, int index);
    Element removeFrom(int index);
}
