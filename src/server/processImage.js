const request = (url, params, callback, fallback) => {
}
const setTimer = (callback, intervalSec) => {
    return setInterval(callback, intervalSec * 1000)
}
const clearTimer = (id) => {
    clearInterval(id)
}
const showMessage = (message, callback) => {
}
const finishProcess = (isDone) => {
}

const RetryConfig = {
    RETRY_INTERVAL: 2,
    MAX_RETRY_TIMES: 20,
}
const UploadStatus = {
    SUCCESS: 0,
    FAILED: 1,
}

const PollingStatus = {
    IN_PROGRESS: 0,
    DONE: 1,
    FAIL: 2,
}

const ServerUrls = {
    UPLOAD: '/upload',
    POLLING: '/polling',
}

const upload = (imageData) => {
    request(ServerUrls.UPLOAD, imageData, (response) => {
        if (response.status === UploadStatus.SUCCESS) {
            polling(imageData.id)
        } else {
            upload(imageData)
        }
    }, (error) => {
        upload(imageData)
    })
}

const polling = (imageId) => {
    let retryTimes = 0
    const timerId = setTimer(() => {
        request(ServerUrls.POLLING, {id: imageId}, (res) => {
            if (res.status === PollingStatus.DONE || res.status === PollingStatus.FAIL) {
                clearTimer(timerId)
                finishProcess(res.status === PollingStatus.DONE)
            } else {
                retryTimes++
                if (retryTimes >= RetryConfig.MAX_RETRY_TIMES) {
                    showMessage('Keep Waiting ?', (shouldKeepWaiting) => {
                        if (!shouldKeepWaiting) {
                            retryTimes = 0
                        } else {
                            clearTimer(timerId)
                            finishProcess(false)
                        }
                    })
                }
            }
        }, (error) => {
            retryTimes++
            if (retryTimes >= RetryConfig.MAX_RETRY_TIMES) {
                showMessage('Keep Waiting ?', (shouldKeepWaiting) => {
                    if (!shouldKeepWaiting) {
                        retryTimes = 0
                    } else {
                        clearTimer(timerId)
                        finishProcess(false)
                    }
                })
            }
        })
    }, RetryConfig.RETRY_INTERVAL)
}

const main = () => {
    const imageData = {
        id: 999,
        image: 'image.jpg',
        type: 3,
    }

    upload(imageData)
}


