# Vivipic Test
### Google API Docs
   1) Reference: https://developers.google.com/photos/library/reference/rest
   2) Query all photos of a user
      - refer API: [GET /v1/mediaItems](https://developers.google.com/photos/library/reference/rest/v1/mediaItems/list)
      - need to provide **pageSize** and **pageToken** to get all photos
   3) Query all albums of a user, and photos in each album
      - query all albums, refer API: [GET /v1/albums](https://developers.google.com/photos/library/reference/rest/v1/albums/list)
      - need to provide **pageSize** and **pageToken** to get all albums
      - to get photos inside each album, refer this API: [POST v1/mediaItems:search](https://developers.google.com/photos/library/reference/rest/v1/mediaItems/search)
      - need to provide **albumId**, **pageToken** and **pageSize** in request body to get all photos 
### Image Processing Queue
- refer to ProcessQueueByArray.java 

### Image Processing API
- refer to processImage.js
